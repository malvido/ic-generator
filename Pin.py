from importlib import reload

import FreeCAD as App
import FreeCADGui as Gui
import Part

import DesignValues
reload(DesignValues)

class Pin:
    def __init__(self,inEngine):
        self.bore       = inEngine.bore
        self.pinGauge   = DesignValues.RodJournalD*inEngine.bore/2
        self.cylinderRadius = inEngine.bore/2.0
        self.pinRadius      = self.pinGauge/2.0
        
    def pin(self):
        App.ActiveDocument.addObject("Part::Cylinder","Pin")
        App.ActiveDocument.ActiveObject.Label = "Pin"
        
        App.ActiveDocument.Pin.Radius = self.pinRadius
        App.ActiveDocument.Pin.Height = self.bore*0.9
        App.ActiveDocument.Pin.Placement = App.Placement(App.Vector(-self.cylinderRadius,0,-self.pinRadius*5/4)
                                                         ,App.Rotation(App.Vector(0,1,0),90)
                                                        )
        App.ActiveDocument.Pin.ViewObject.ShapeColor = (0.9,0.8,0.4)
        
    def build(self,inName):
        
        self.docName=inName
        
        App.newDocument(self.docName)
        App.ActiveDocument=App.getDocument(self.docName)
        Gui.ActiveDocument=App.getDocument(self.docName)
        
        self.pin()
        
        Gui.SendMsgToActiveView("ViewFit")
        Gui.activeDocument().activeView().viewAxonometric()
  
