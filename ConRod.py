from importlib import reload
import FreeCAD as App
import FreeCADGui as Gui
import Part

import DesignValues
reload(DesignValues)

class ConRod:
    def __init__(self,inEngine):
        self.bore       = inEngine.bore
        self.stroke     = inEngine.stroke
        self.pinGauge   = DesignValues.RodJournalD*inEngine.bore/2
        self.rodStroke  = inEngine.rodStroke
        
        self.camGauge   = DesignValues.RodJournalD*inEngine.bore
        self.rodLength  = inEngine.stroke*inEngine.rodStroke
        self.pinRadius  = self.pinGauge/2.0
        self.camRadius  = self.camGauge/2.0
        self.width  = inEngine.bore*DesignValues.RodJournalW
        
    def arm(self):
        App.ActiveDocument.addObject("Part::Box","Arm")
        App.ActiveDocument.ActiveObject.Label = "Arm"
        
        App.ActiveDocument.Arm.Height = self.width
        App.ActiveDocument.Arm.Width = self.pinGauge
        App.ActiveDocument.Arm.Length = self.rodLength
        App.ActiveDocument.Arm.Placement = App.Placement(App.Vector(0,-self.pinGauge/2,0)
                                                         ,App.Rotation(App.Vector(0,0,1),0))
        
        App.ActiveDocument.addObject("Part::Cylinder","RodHead")
        App.ActiveDocument.ActiveObject.Label = "RodHead"
        
        App.ActiveDocument.RodHead.Radius = self.pinGauge*5/8
        App.ActiveDocument.RodHead.Height = self.width
        App.ActiveDocument.RodHead.Placement = App.Placement(App.Vector(self.rodLength,0,0)
                                                              ,App.Rotation(App.Vector(0,0,1),0))
        
        App.ActiveDocument.addObject("Part::Cylinder","RodTail")
        App.ActiveDocument.ActiveObject.Label = "RodTail"
        
        App.ActiveDocument.RodTail.Radius = self.bore*(DesignValues.MainJournalD/2+DesignValues.FilletRadius)
        App.ActiveDocument.RodTail.Height = self.width
        App.ActiveDocument.RodTail.Placement = App.Placement(App.Vector(0,0,0)
                                                              ,App.Rotation(App.Vector(0,0,1),0))
       
        App.ActiveDocument.addObject("Part::MultiFuse","ConRaw")
        App.ActiveDocument.ConRaw.Shapes = [App.ActiveDocument.Arm
                                            ,App.ActiveDocument.RodHead
                                            ,App.ActiveDocument.RodTail
                                           ]

        App.ActiveDocument.recompute()
        
    def tool(self):
        App.ActiveDocument.addObject("Part::Cylinder","HeadTool")
        App.ActiveDocument.ActiveObject.Label = "HeadTool"

        App.ActiveDocument.HeadTool.Radius = self.pinGauge/2
        App.ActiveDocument.HeadTool.Height = self.width
        App.ActiveDocument.HeadTool.Placement = App.Placement(App.Vector(self.rodLength,0,0)
                                                              ,App.Rotation(App.Vector(0,0,1),0)
                                                             )

        App.ActiveDocument.addObject("Part::Cut","ConRaw1")
        App.ActiveDocument.ConRaw1.Base = App.ActiveDocument.ConRaw
        App.ActiveDocument.ConRaw1.Tool = App.ActiveDocument.HeadTool
        
        App.ActiveDocument.addObject("Part::Cylinder","TailTool")
        App.ActiveDocument.ActiveObject.Label = "TailTool"

        App.ActiveDocument.TailTool.Radius = self.camGauge/2
        App.ActiveDocument.TailTool.Height = self.width
        App.ActiveDocument.TailTool.Placement = App.Placement(App.Vector(0,0,0)
                                                              ,App.Rotation(App.Vector(0,0,1),0)
                                                             )

        App.ActiveDocument.addObject("Part::Cut","TooledCon")
        App.ActiveDocument.TooledCon.Base = App.ActiveDocument.ConRaw1
        App.ActiveDocument.TooledCon.Tool = App.ActiveDocument.TailTool
        
        App.ActiveDocument.recompute()
        
    def polish(self):
        App.ActiveDocument.addObject("Part::Fillet","ConRod")
        App.ActiveDocument.ConRod.Base = App.ActiveDocument.TooledCon
        
        __fillets__ = []
        __fillets__.append((2,self.bore/4,self.bore/4))
        __fillets__.append((4,self.bore/4,self.bore/4))
        __fillets__.append((12,self.bore/4,self.bore/4))
        __fillets__.append((18,self.bore/4,self.bore/4))
        
        App.ActiveDocument.ConRod.Edges=__fillets__
        
        App.ActiveDocument.recompute()

        del __fillets__
        
        # fix visibility bug
        Gui.getDocument("ConRod").TooledCon.Visibility=False
        App.ActiveDocument.ConRod.ViewObject.ShapeColor = (0.1,0.9,0.1)
        # 4 18
        
    def build(self,inName):
        
        self.docName=inName
        
        App.newDocument(self.docName)
        App.ActiveDocument=App.getDocument(self.docName)
        Gui.ActiveDocument=App.getDocument(self.docName)
        
        self.arm()
        self.tool()
        self.polish()
        
        Gui.SendMsgToActiveView("ViewFit")
        Gui.activeDocument().activeView().viewAxonometric()
    