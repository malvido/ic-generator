from importlib import reload

import FreeCAD as App
import FreeCADGui as Gui
import Part

import DesignValues
reload(DesignValues)

class Piston:
    def __init__(self,inEngine):
        engine=inEngine
        if inEngine.engineType == "ED":
            engine = inEngine.secondary

        self.bore       = engine.bore
        self.stroke     = engine.stroke
        self.pinGauge   = DesignValues.RodJournalD*inEngine.bore/2
        
        self.pistonClearance = 0.002
        
        self.numberOfRings = 3
        
        self.headHeight     = engine.stroke/2.0
        self.skirtHeight    = self.headHeight/4.0
        self.pistonRadius   = engine.bore/2.0 - inEngine.ringGauge
        self.ringHeight     = self.headHeight/7
        self.pinRadius      = self.pinGauge/2.0
        
        self.holderSide  = DesignValues.RodJournalW*self.bore*5/4
        self.holderToolSide  = DesignValues.RodJournalW*self.bore
        self.holderGauge = round(engine.pinGauge)
    
    def solve(self):
        
        if self.bore<0:
            self.bore=self.stroke
        if self.stroke<0: 
            self.stroke=self.bore
        
        if self.bore<0: 
            return false
        
        if self.pinGauge<0:
            self.pinGauge=round(self.bore/3.1)
            
        # piston clearance: https://www.smokstak.com/forum/showthread.php?t=56403
        self.pistonClearance = 0.025+self.bore*0.001
        
        self.pistonRadius   = self.bore/2.0-self.pistonClearance
        
        if self.headHeight<0:
            self.headHeight     = self.stroke/2.0
        
        if self.headHeight<0:
            self.skirtHeight    = self.stroke-self.headHeight
            
        self.ringHeight     = self.headHeight/(self.numberOfRings*2+1)
        
        self.pinRadius      = self.pinGauge/2.0
        
        self.holderGauge  = round(self.pinGauge)
        
        return True
        
    def head(self):
        App.ActiveDocument.addObject("Part::Cylinder","PistonHeadPlain")
        App.ActiveDocument.ActiveObject.Label = "PistonHeadPlain"
        
        App.ActiveDocument.PistonHeadPlain.Radius = self.pistonRadius
        App.ActiveDocument.PistonHeadPlain.Height = self.headHeight
        
        App.ActiveDocument.getObject("PistonHeadPlain").ViewObject.ShapeColor = (0.9,0.1,0.1)

        App.ActiveDocument.recompute()
        
    def ring(self,inName):
        solid=inName+"Solid"
        App.ActiveDocument.addObject("Part::Cylinder",solid)
        App.ActiveDocument.ActiveObject.Label = solid
        
        App.ActiveDocument.getObject(solid).Radius = self.pistonRadius+self.pistonClearance
        App.ActiveDocument.getObject(solid).Height = self.ringHeight
        
        App.ActiveDocument.getObject(solid).Placement = App.Placement(App.Vector(0,0,0)
                                                           ,App.Rotation(App.Vector(0,0,1),0)
                                                          )
        App.ActiveDocument.getObject(solid).ViewObject.ShapeColor = (0.5,0.5,0.5)
        
        tool=inName+"Tool"
        App.ActiveDocument.addObject("Part::Cylinder",tool)
        App.ActiveDocument.ActiveObject.Label = tool
        
        App.ActiveDocument.getObject(tool).Radius = self.pistonRadius-self.pistonClearance
        App.ActiveDocument.getObject(tool).Height = self.ringHeight
        
        App.ActiveDocument.getObject(tool).Placement = App.Placement(App.Vector(0,0,0)
                                                           ,App.Rotation(App.Vector(0,0,1),0)
                                                          )
        App.ActiveDocument.getObject(tool).ViewObject.ShapeColor = (0.5,0.5,0.5)
        
        App.ActiveDocument.addObject("Part::Cut",inName)
        App.ActiveDocument.getObject(inName).Base = App.ActiveDocument.getObject(solid)
        App.ActiveDocument.getObject(inName).Tool = App.ActiveDocument.getObject(tool)
       
        App.ActiveDocument.getObject(inName).ViewObject.ShapeColor = (0.5,0.5,0.5)
        
    def rings(self):
        
        for i in range(self.numberOfRings):
            lRingName="Ring"+str(i)
            self.ring(lRingName)
            App.ActiveDocument.getObject(lRingName).Placement = App.Placement(App.Vector(0,0,self.ringHeight*2*i)
                                                           ,App.Rotation(App.Vector(0,0,1),0)
                                                          )
            App.ActiveDocument.getObject(lRingName).ViewObject.ShapeColor = (0.5,0.5,0.5)
        
        
        App.ActiveDocument.recompute()
    
    def skirt(self):
        App.ActiveDocument.addObject("Part::Cylinder","SkirtBase")
        App.ActiveDocument.ActiveObject.Label = "SkirtBase"
        
        App.ActiveDocument.SkirtBase.Radius = self.pistonRadius
        App.ActiveDocument.SkirtBase.Height = self.skirtHeight
        App.ActiveDocument.SkirtBase.Placement = App.Placement(App.Vector(0,0,-self.skirtHeight)
                                                               ,App.Rotation(App.Vector(0,0,1),0)
                                                              )
        App.ActiveDocument.getObject("SkirtBase").ViewObject.ShapeColor = (0.9,0.1,0.1)

        App.ActiveDocument.addObject("Part::Cylinder","SkirtTool")
        App.ActiveDocument.ActiveObject.Label = "SkirtTool"

        App.ActiveDocument.SkirtTool.Radius = self.pistonRadius*(7.0/8.0)
        App.ActiveDocument.SkirtTool.Height = self.skirtHeight
        App.ActiveDocument.SkirtTool.Placement = App.Placement(App.Vector(0,0,-self.skirtHeight)
                                                               ,App.Rotation(App.Vector(0,0,1),0)
                                                              )
        App.ActiveDocument.getObject("SkirtTool").ViewObject.ShapeColor = (0.9,0.1,0.1)

        App.ActiveDocument.addObject("Part::Cut","Skirt")
        App.ActiveDocument.Skirt.Base = App.ActiveDocument.SkirtBase
        App.ActiveDocument.Skirt.Tool = App.ActiveDocument.SkirtTool
        App.ActiveDocument.getObject("Skirt").ViewObject.ShapeColor = (0.9,0.1,0.1)

        App.ActiveDocument.recompute()

    def holder(self):
        App.ActiveDocument.addObject("Part::Box","HolderBox")
        App.ActiveDocument.ActiveObject.Label = "HolderBox"
        
        App.ActiveDocument.HolderBox.Height = self.holderSide*2
        App.ActiveDocument.HolderBox.Width = self.holderSide
        App.ActiveDocument.HolderBox.Length = self.bore
        
        App.ActiveDocument.HolderBox.Placement = App.Placement(App.Vector(-self.bore/2,-self.holderSide/2,-self.holderSide)
                                                                ,App.Rotation(App.Vector(0,0,1),0)
                                                               )
        
        App.ActiveDocument.addObject("Part::Cylinder","HolderRing")
        App.ActiveDocument.ActiveObject.Label = "HolderRing"
        
        App.ActiveDocument.HolderRing.Radius = self.pistonRadius
        App.ActiveDocument.HolderRing.Height = self.holderSide*2
        
        App.ActiveDocument.HolderRing.Placement = App.Placement(App.Vector(0,0,-self.holderSide)
                                                           ,App.Rotation(App.Vector(0,0,1),0)
                                                          )
        App.ActiveDocument.getObject("HolderRing").ViewObject.ShapeColor = (0.9,0.1,0.1)

        App.ActiveDocument.addObject("Part::MultiCommon","HolderBar")
        App.ActiveDocument.HolderBar.Shapes = [App.ActiveDocument.HolderBox
                                              ,App.ActiveDocument.HolderRing
                                             ]
        App.ActiveDocument.getObject("HolderBar").ViewObject.ShapeColor = (0.9,0.1,0.1)

        App.ActiveDocument.addObject("Part::Box","HolderTool")
        App.ActiveDocument.ActiveObject.Label = "HolderTool"
        
        App.ActiveDocument.HolderTool.Height = self.holderSide
        App.ActiveDocument.HolderTool.Width = self.holderSide
        App.ActiveDocument.HolderTool.Length = self.holderToolSide
        
        App.ActiveDocument.HolderTool.Placement = App.Placement(App.Vector(-self.holderToolSide/2,-self.holderSide/2,-self.holderSide)
                                                                 ,App.Rotation(App.Vector(0,0,1),0)
                                                                )
        App.ActiveDocument.getObject("HolderTool").ViewObject.ShapeColor = (0.9,0.1,0.1)

        App.ActiveDocument.addObject("Part::Cut","Holder")
        App.ActiveDocument.Holder.Base = App.ActiveDocument.HolderBar
        App.ActiveDocument.Holder.Tool = App.ActiveDocument.HolderTool
        App.ActiveDocument.getObject("Holder").ViewObject.ShapeColor = (0.9,0.1,0.1)

        
        App.ActiveDocument.recompute()

    def pinTool(self):
        App.ActiveDocument.addObject("Part::Cylinder","PinTool")
        App.ActiveDocument.ActiveObject.Label = "PinTool"
        
        App.ActiveDocument.PinTool.Radius = self.pinRadius
        App.ActiveDocument.PinTool.Height = self.bore
        App.ActiveDocument.PinTool.Placement = App.Placement(App.Vector(-self.pistonRadius-self.pistonClearance,0,-self.holderSide/2)
                                                         ,App.Rotation(App.Vector(0,1,0),90)
                                                        )

        App.ActiveDocument.getObject("PinTool").ViewObject.ShapeColor = (0.9,0.1,0.1)

        
    def join(self):
        
        App.ActiveDocument.addObject("Part::MultiFuse","Weld")
        App.ActiveDocument.Weld.Shapes = [App.ActiveDocument.PistonHeadPlain
                                          ,App.ActiveDocument.Skirt
                                          ,App.ActiveDocument.Holder
                                         ]
        App.ActiveDocument.getObject("Weld").ViewObject.ShapeColor = (0.9,0.1,0.1)

        App.ActiveDocument.addObject("Part::Cut","Unpolished")
        App.ActiveDocument.Unpolished.Base = App.ActiveDocument.Weld
        App.ActiveDocument.Unpolished.Tool = App.ActiveDocument.PinTool

        App.ActiveDocument.getObject("Unpolished").ViewObject.ShapeColor = (0.9,0.1,0.1)

        App.ActiveDocument.recompute()
    
    def polish(self,edges):
        App.ActiveDocument.addObject("Part::Fillet","Polished")
        App.ActiveDocument.getObject("Polished").Base = App.ActiveDocument.getObject("Unpolished")
        
        
        __fillets__ = []
        for edge in edges:
            __fillets__.append((edge,1,1))

        App.ActiveDocument.getObject("Polished").Edges=__fillets__
        
        App.ActiveDocument.getObject("Polished").ViewObject.ShapeColor = (0.9,0.1,0.1)
        
        # fix visibility bug
        Gui.getDocument(self.docName).getObject("Unpolished").Visibility=False
        
        App.ActiveDocument.recompute()
        
    def addRings(self):

        lShapes=[App.ActiveDocument.getObject("Polished")]
        
        for i in range(self.numberOfRings):
            lShapes.append(App.ActiveDocument.getObject("Ring"+str(i)))

        App.ActiveDocument.addObject("Part::MultiFuse","Piston")
        App.ActiveDocument.getObject("Piston").Shapes = lShapes

        # fix visibility bug
        Gui.getDocument(self.docName).getObject("Polished").Visibility=False
        
        App.ActiveDocument.recompute()

    def build(self,inName):
        
        self.solve()
        
        self.docName=inName
        
        App.newDocument(self.docName)
        App.ActiveDocument=App.getDocument(self.docName)
        Gui.ActiveDocument=App.getDocument(self.docName)
        
        self.head()
        self.rings()
        self.skirt()
        self.holder()
        self.pinTool()
        self.join()
        #self.polish([1,4,6,9,10,12,14,20,24,30,32,34,36,37,38])
        self.polish([18,20,21,27])
        self.addRings()

        Gui.SendMsgToActiveView("ViewFit")
        Gui.activeDocument().activeView().viewAxonometric()
