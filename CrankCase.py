from importlib import reload

import FreeCAD as App
import FreeCADGui as Gui
import Part

import DesignValues
reload(DesignValues)

class CrankCase:
    def __init__(self,inEngine):
        self.stroke     = inEngine.stroke
        self.bore       = inEngine.bore
        self.gauge      = DesignValues.MainJournalD*inEngine.bore
        self.pinGauge   = DesignValues.RodJournalD*inEngine.bore/2
        self.holderSide  = DesignValues.RodJournalW*self.bore*5/4
        self.cylHeight  = self.stroke*inEngine.rodStroke+self.stroke+self.holderSide/2
        self.thickDelta = round(self.bore*0.1/2)
    
        self.caseLength = inEngine.crankLen
        
    def block(self):
        App.ActiveDocument.addObject("Part::Cylinder","CylinderSolid")
        App.ActiveDocument.ActiveObject.Label = "CylinderSolid"
        
        App.ActiveDocument.CylinderSolid.Radius = self.bore/2+self.thickDelta
        App.ActiveDocument.CylinderSolid.Height = self.cylHeight
        
        App.ActiveDocument.addObject("Part::Cylinder","CaseSolid")
        App.ActiveDocument.ActiveObject.Label = "CaseSolid"
        
        App.ActiveDocument.CaseSolid.Radius = self.stroke/2+self.bore/2+self.thickDelta
        App.ActiveDocument.CaseSolid.Height = self.caseLength
        
        App.ActiveDocument.CaseSolid.Placement = App.Placement(App.Vector(-self.caseLength/2,0,0)
                                                         ,App.Rotation(App.Vector(0,1,0),90)
                                                        )
    
        App.ActiveDocument.addObject("Part::MultiFuse","BlockSolid")
        App.ActiveDocument.BlockSolid.Shapes = [App.ActiveDocument.CylinderSolid
                                                ,App.ActiveDocument.CaseSolid
                                               ]

        App.ActiveDocument.recompute()
        
    def tool(self):
        App.ActiveDocument.addObject("Part::Cylinder","CylinderTool")
        App.ActiveDocument.ActiveObject.Label = "CylinderTool"
        
        App.ActiveDocument.CylinderTool.Radius = self.bore/2
        App.ActiveDocument.CylinderTool.Height = self.cylHeight
        
        App.ActiveDocument.addObject("Part::Cut","ToolCylinder")
        App.ActiveDocument.ToolCylinder.Base = App.ActiveDocument.BlockSolid
        App.ActiveDocument.ToolCylinder.Tool = App.ActiveDocument.CylinderTool
        
        
        App.ActiveDocument.addObject("Part::Cylinder","CaseTool")
        App.ActiveDocument.ActiveObject.Label = "CaseTool"
        
        App.ActiveDocument.CaseTool.Radius = self.stroke/2+self.bore/2
        App.ActiveDocument.CaseTool.Height = self.caseLength-self.thickDelta
        
        App.ActiveDocument.CaseTool.Placement = App.Placement(App.Vector(-(self.caseLength-self.thickDelta)/2,0,0)
                                                         ,App.Rotation(App.Vector(0,1,0),90)
                                                        )
        
        App.ActiveDocument.addObject("Part::Cut","ToolCase")
        App.ActiveDocument.ToolCase.Base = App.ActiveDocument.ToolCylinder
        App.ActiveDocument.ToolCase.Tool = App.ActiveDocument.CaseTool
        
        App.ActiveDocument.addObject("Part::Cylinder","ShaftTool")
        App.ActiveDocument.ActiveObject.Label = "ShaftTool"
        
        App.ActiveDocument.ShaftTool.Radius = self.gauge/2.0
        App.ActiveDocument.ShaftTool.Height = self.caseLength
        
        App.ActiveDocument.ShaftTool.Placement = App.Placement(App.Vector(-self.caseLength/2,0,0)
                                                         ,App.Rotation(App.Vector(0,1,0),90)
                                                        )
        
        App.ActiveDocument.addObject("Part::Cut","CrankCase")
        App.ActiveDocument.CrankCase.Base = App.ActiveDocument.ToolCase
        App.ActiveDocument.CrankCase.Tool = App.ActiveDocument.ShaftTool
        
        App.ActiveDocument.recompute()
    
    def sideCut(self):
        App.ActiveDocument.addObject("Part::Box","CutTool")
        App.ActiveDocument.ActiveObject.Label = "CutTool"
        
        App.ActiveDocument.CutTool.Height = self.cylHeight
        App.ActiveDocument.CutTool.Width = self.cylHeight
        App.ActiveDocument.CutTool.Length = self.cylHeight
        
        App.ActiveDocument.addObject("Part::Cut","CutCase")
        App.ActiveDocument.CutCase.Base = App.ActiveDocument.CrankCase
        App.ActiveDocument.CutCase.Tool = App.ActiveDocument.CutTool
        
        App.ActiveDocument.recompute()
    
    def build(self,inName):
        
        self.docName=inName
        
        App.newDocument(self.docName)
        App.ActiveDocument=App.getDocument(self.docName)
        Gui.ActiveDocument=App.getDocument(self.docName)
        
        self.block()
        self.tool()
        self.sideCut()
        
        Gui.SendMsgToActiveView("ViewFit")
        Gui.activeDocument().activeView().viewAxonometric()
 