
'''
Cylinder bore diameter = D
Cylinder centre distance = 1.20 D

Main journal diameter = 0.75 D
Main journal width = 0.40 D

Rod journals diameter = 0.65 D
Rod journal width = 0.35 D

Web thickness = 0.25 D
Fillet radius of journal and webs = 0.04 D

'''

MainJournalD = 0.75
MainJournalW = 0.40
RodJournalD  = 0.65
RodJournalW  = 0.35
WebWidth     = 0.25
FilletRadius = 0.04
