# -*- coding: utf-8 -*-

from PySide import QtGui, QtCore

class EngineForm(QtGui.QDialog):
    """"""
    def __init__(self):
        super(EngineForm, self).__init__()
        
        self.userCancelled	= "Cancelled"
        self.userOK			= "OK"
        
        self.initUI()
        
    def initUI(self):
        self.result = self.userCancelled
        
        # create our window
        # define window		xLoc,yLoc,xDim,yDim
        self.setGeometry(	250, 250, 400, 350)
        self.setWindowTitle("Engine Characteristics")
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        
        # create some Labels & input boxes
        self.label1 = QtGui.QLabel("Bore (mm)", self)
        self.label1.setFont('Courier') # set to a non-proportional font
        self.label1.move(20, 20)
        
        self.boreInput = QtGui.QLineEdit(self)
        self.boreInput.setText("80")
        self.boreInput.setFixedWidth(190)
        self.boreInput.move(20, 40)
        
        self.label2 = QtGui.QLabel("Stroke (mm)", self)
        self.label2.setFont('Courier') # set to a non-proportional font
        self.label2.move(20, 70)
        
        self.strokeInput = QtGui.QLineEdit(self)
        self.strokeInput.setText("80")
        self.strokeInput.setFixedWidth(190)
        self.strokeInput.move(20, 90)
        
        self.label3 = QtGui.QLabel("Rod/Stroke Ratio", self)
        self.label3.setFont('Courier') # set to a non-proportional font
        self.label3.move(20, 120)
        
        self.rodStrokeInput = QtGui.QLineEdit(self)
        self.rodStrokeInput.setText("1.75")
        self.rodStrokeInput.setFixedWidth(190)
        self.rodStrokeInput.move(20, 140)
        
        # cancel button
        cancelButton = QtGui.QPushButton('Cancel', self)
        cancelButton.clicked.connect(self.onCancel)
        cancelButton.setAutoDefault(True)
        cancelButton.move(150, 280)
        
		# OK button
        okButton = QtGui.QPushButton('OK', self)
        okButton.clicked.connect(self.onOk)
        okButton.move(260, 280)
		
        # now make the window visible
        self.show()
    def onCancel(self):
        self.result			= self.userCancelled
        self.close()
    def onOk(self):
        self.result			= self.userOK
        self.close()
	
