from importlib import reload

import FreeCAD as App
import FreeCADGui as Gui

import Part

import DesignValues
reload(DesignValues)

class CrankShaft:
    def __init__(self,inEngine):
        self.engine=inEngine

        self.stroke     = inEngine.stroke
        self.gauge      = inEngine.crankGauge
        self.thickDelta = round(inEngine.pinGauge)

    def addJournal(self,name,journalType,px,py,pz,rx,ry,rz):
        App.ActiveDocument.addObject("Part::Cylinder",name)
        App.ActiveDocument.ActiveObject.Label = name

        diameter=DesignValues.MainJournalD
        width=DesignValues.MainJournalW+DesignValues.WebWidth
        if journalType =="rod":
            diameter=DesignValues.RodJournalD
            width=DesignValues.RodJournalW+DesignValues.WebWidth*2
            
        App.ActiveDocument.getObject(name).Radius = (self.engine.bore*diameter)/2.0
        App.ActiveDocument.getObject(name).Height = self.engine.bore*width
        App.ActiveDocument.getObject(name).Placement = App.Placement(App.Vector(px,py,pz)
                                                              ,App.Rotation(App.Vector(rx,ry,rz),0))
        

    def addWeb(self,name,px,py,pz,rx,ry,rz):
        App.ActiveDocument.addObject("Part::Box",name+"Box")
        App.ActiveDocument.ActiveObject.Label = name+"Box"
        
        App.ActiveDocument.getObject(name+"Box").Height = self.engine.bore*(DesignValues.WebWidth)
        App.ActiveDocument.getObject(name+"Box").Width = self.engine.bore*(DesignValues.MainJournalD+DesignValues.FilletRadius*2)
        App.ActiveDocument.getObject(name+"Box").Length = self.engine.stroke/2
        App.ActiveDocument.getObject(name+"Box").Placement = App.Placement(App.Vector(px,py,pz)
                                                              ,App.Rotation(App.Vector(rx,ry,rz),0))

        App.ActiveDocument.addObject("Part::Cylinder",name+"Cyl0")
        App.ActiveDocument.ActiveObject.Label = name+"Cyl0"
        radius=(DesignValues.MainJournalD/2+DesignValues.FilletRadius)
        App.ActiveDocument.getObject(name+"Cyl0").Radius = self.engine.bore*radius
        App.ActiveDocument.getObject(name+"Cyl0").Height = self.engine.bore*(DesignValues.WebWidth)
        App.ActiveDocument.getObject(name+"Cyl0").Placement = App.Placement(App.Vector(px,0,pz)
                                                              ,App.Rotation(App.Vector(rx,ry,rz),0))
        
        App.ActiveDocument.addObject("Part::Cylinder",name+"Cyl1")
        App.ActiveDocument.ActiveObject.Label = name+"Cyl1"
        App.ActiveDocument.getObject(name+"Cyl1").Radius = self.engine.bore*radius
        App.ActiveDocument.getObject(name+"Cyl1").Height = self.engine.bore*(DesignValues.WebWidth)
        App.ActiveDocument.getObject(name+"Cyl1").Placement = App.Placement(App.Vector(self.stroke/2,0,pz)
                                                              ,App.Rotation(App.Vector(rx,ry,rz),0))
        
        App.ActiveDocument.addObject("Part::MultiFuse",name)
        App.ActiveDocument.getObject(name).Shapes = [App.ActiveDocument.getObject(name+"Box")
                                                ,App.ActiveDocument.getObject(name+"Cyl0")
                                                ,App.ActiveDocument.getObject(name+"Cyl1")
                                               ]

    def shaft(self):
        self.addJournal("MJ1","main",0,0,0,0,0,1)
        self.addWeb("W1",0,-self.engine.bore*(DesignValues.MainJournalD/2+DesignValues.FilletRadius),self.engine.bore*DesignValues.MainJournalW,0,0,1)
        self.addJournal("RJ","rod",self.stroke/2,0,self.engine.bore*DesignValues.MainJournalW,0,0,1)
        self.addWeb("W2",0,-self.engine.bore*(DesignValues.MainJournalD/2+DesignValues.FilletRadius),self.engine.bore*(DesignValues.MainJournalW+DesignValues.RodJournalW+DesignValues.WebWidth),0,0,1)
        self.addJournal("MJ2","main",0,0,self.engine.bore*(DesignValues.MainJournalW+DesignValues.RodJournalW+DesignValues.WebWidth),0,0,1)

        App.ActiveDocument.addObject("Part::MultiFuse","Coarse")
        App.ActiveDocument.getObject("Coarse").Shapes = [App.ActiveDocument.getObject("MJ1")
                                                ,App.ActiveDocument.getObject("W1")
                                                ,App.ActiveDocument.getObject("RJ")
                                                ,App.ActiveDocument.getObject("W2")
                                                ,App.ActiveDocument.getObject("MJ2")
                                               ]


    def smooth(self,edges):
        App.ActiveDocument.addObject("Part::Fillet","Crankshaft")
        App.ActiveDocument.getObject("Crankshaft").Base = App.ActiveDocument.getObject("Coarse")
        
        
        __fillets__ = []
        for edge in edges:
            __fillets__.append((edge,1,1))

        App.ActiveDocument.getObject("Crankshaft").Edges=__fillets__
        App.ActiveDocument.recompute()
        del __fillets__

    def build(self,inName):
        
        self.docName=inName
        
        App.newDocument(self.docName)
        App.ActiveDocument=App.getDocument(self.docName)
        Gui.ActiveDocument=App.getDocument(self.docName)
        
        self.shaft()

        self.smooth([1,3,5,7,8,9,10,12,13,14,18,20,22,24,25,26,27,29,30,31])

        Gui.getDocument("CrankShaft").getObject("Coarse").Visibility=False

        App.ActiveDocument.getObject("Crankshaft").ViewObject.ShapeColor = (0.9,0.8,0.4)

        Gui.SendMsgToActiveView("ViewFit")
        Gui.activeDocument().activeView().viewAxonometric()
  