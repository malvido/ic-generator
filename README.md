**Disclaimer**

It is sad to live in a world that needs this type of disclaimers, but it's the world we live in:

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

Straight out from the license to make it really clear.

<img src="Trad3.png" alt="You should see a sample output here.">

**Description**

IC Generator is an addon for [FreeCAD](https://www.freecadweb.org/) to automate the creation of piston-driven internal combustion engines.

**Usage**

Install FreeCAD if you have not already done so.
Clone the project and move all the _.py_ files into your FreeCAD Macro folder. The location of the folder can be found at the bottom of the Macro Editor dialog.

Invoke by executing the MotorParts.py script.

**Goals**

1. Generate the parts for an Internal Combustion engine block given a set of parameters.
2. Usage through a Graphical User Interface.
3. Assemble an engine with the generated parts.
4. Animate the engine.

**Status**

Goals 1 and 2 have been achieved. This does not necessarily mean that there is no room for improvement. As a matter of fact, there is a lot of room for improvement.
Goal 3 has to be done manually. I am currently using the [A2Plus](https://wiki.freecadweb.org/A2plus_Workbench) workbench. Probably the workbench [Assembly4](https://wiki.freecadweb.org/Assembly4_Workbench) would be more suitable but it still does not work with the current stable version of FreeCAD.
Goal 4 was somehow achieved "on paper" but the animation rate was so poor that I am exploring alternative ways to achieve it.

**Help**

Goal 3 is very tricky as the assembly process requires matching of features like edges and surfaces from different parts and the numbering of these varies widely when you make changes to the parts. So if anyone knows a way around this, please do tell. It will be greatly appreciated (no, not with money).

**Caveats**

FreeCAD has a very annoying tendency to cache the scripts it runs. So if you modify the code for your own purpose and learning, be sure to uncomment the _reload_ lines at the beginning of the scripts to make sure your changes get reloaded and executed.

**Why**

*Just because...*

I am an electrical engineer by degree, a software engineer by career and a mechanical engineer by hobby.

*Why FreeCAD?*

Because it is open source, technically oriented and has a future. [Blender](https://www.blender.org/) would have been better for animation but worse for technical drawings. [Fusion 360](https://www.autodesk.com/products/fusion-360/overview) would have been better for everything but AutoDesk is already cutting down the features of the community edition so it would have been worse for the future.
