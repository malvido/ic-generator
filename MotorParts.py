# -*- coding: utf-8 -*-

# Macro Begin: /Users/jose/Library/Preferences/FreeCAD/Macro/Piston.FCMacro +++++++++++++++++++++++++++++++++++++++++++++++++
from importlib import reload
import FreeCAD
import Part

import MotorGUI
#reload(MotorGUI)

import Piston
#reload(Piston)

import ConRod
#reload(ConRod)

import Pin
#reload(Pin)

import CrankShaft
#reload(CrankShaft)

import CrankCase
#reload(CrankCase)

import Engine
#reload(Engine)

import DesignValues
#reload(DesignValues)

form=MotorGUI.EngineForm()
form.exec_()

if form.result==form.userCancelled:
	exit() # steps to handle user clicking Cancel

engine = Engine.Engine(float(form.boreInput.text())
                  ,float(form.strokeInput.text())
                  ,float(form.rodStrokeInput.text())
                 )

Gui.activateWorkbench("PartWorkbench")

crankCase=CrankCase.CrankCase(engine)
crankCase.build("CrankCase")

piston=Piston.Piston(engine)
piston.build("Piston")

conrod=ConRod.ConRod(engine)
conrod.build("ConRod")

crankshaft=CrankShaft.CrankShaft(engine)
crankshaft.build("CrankShaft")

pin=Pin.Pin(engine)
pin.build("Pin")


''''
'''
